//
//  ContentView.swift
//  Calculator
//
//  Created by cqlapple on 13/06/21.
//

import SwiftUI

enum CalcButton:String {
    case one = "1"
    case two = "2"
    case three = "3"
    case four = "4"
    case five = "5"
    case six = "6"
    case seven = "7"
    case eight = "8"
    case nine = "9"
    case zero = "0"
    case add =  "+"
    case subtract = "-"
    case multiply =  "x"
    case devide =  "/"
    case equal = "="
    case clear = "AC"
    case decimal = "."
    case precent =  "%"
    case negtive =  "+/-"
    
    
    var buttonColor:Color{
        switch self {
        case .add,.subtract,.multiply,.devide,.equal:
            return .orange
        case .clear,.negtive,.precent:
            return Color(.lightGray)
        default:
            return .secondary
        }
    }
}

enum Operation{
    case add,subtract,multiply,devide,equal,none
}



struct ContentView: View {
    
    
    @State var value = "0"
    @State var currentOperation:Operation = .none
    @State var runningNumber = 0
    
    
    let buttons:[[CalcButton]] = [
        [.clear,.negtive,.precent,.devide],
        [.seven,.eight,.nine,.multiply],
        [.four,.five,.six,.subtract],
        [.one,.two,.three,.add],
        [.zero,.decimal,.equal],
    ]
    
    
    
    var body: some View {
        ZStack{
            Color.black.edgesIgnoringSafeArea(.all)
            
            VStack{
                Text("Basic Calculator")
                    .font(.system(size: 30, weight: .medium, design: .default))
                    .foregroundColor(.orange)
                    .frame(width: 300, height: 50, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                Spacer()
                HStack{
                    Spacer()
                    Text(value)
                        .font(.system(size: 73))
                        .foregroundColor(.white)
                        .minimumScaleFactor(0.5)
                                  .lineLimit(2)
                }.padding(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/, /*@START_MENU_TOKEN@*/10/*@END_MENU_TOKEN@*/)
                
                ForEach(buttons, id: \.self) { rows in
                    HStack(spacing: 10){
                        ForEach(rows, id: \.self) {  items in
                            
                            Button(action: {
                                didTap(button: items)
                            }, label: {
                                Text(items.rawValue)
                                    .font(.system(size: 32, weight: .medium, design: .default))
                                    .frame(width: self.buttonWidth(item: items), height: buttonHeight(), alignment: .center)
                                    .foregroundColor(.white)
                                    .background(items.buttonColor) .cornerRadius(self.buttonWidth(item: items) / 2)
                            })
                            
                        }
                        
                    }.padding(.bottom, 2)
                    
                }.padding(.bottom, 2)
                
            }
            
        }
    }
    
    
    func buttonWidth(item:CalcButton) -> CGFloat{
        if item == .zero{
            return ((UIScreen.main.bounds.width - (5*12)) / 2)
        }else{
            return ((UIScreen.main.bounds.width - (5*12)) / 4)
        }
    }
    
    func buttonHeight() -> CGFloat{
        return ((UIScreen.main.bounds.width - (5*12)) / 4)
    }
    
    func didTap(button:CalcButton){
        
        switch button {
        case .add,.subtract,.multiply,.devide,.equal:
            if button == .add{
                self.currentOperation = .add
                self.runningNumber = Int(value) ?? 0
            }
            
            else if button == .subtract{
                self.currentOperation = .subtract
                self.runningNumber = Int(value) ?? 0
            }
            
            else if button == .multiply{
                self.currentOperation = .multiply
                self.runningNumber = Int(value) ?? 0
            }
            else if button == .devide{
                self.currentOperation = .devide
                self.runningNumber = Int(value) ?? 0
            }
            
            else if button == .equal{
               
                let ruuningVal = self.runningNumber
                let currentVal = Int(self.value) ?? 0
                switch self.currentOperation {
                case .add:
                    self.value = "\(ruuningVal + currentVal)"
                case .subtract:
                    self.value = "\(ruuningVal - currentVal)"
                case .multiply:
                    self.value = "\(ruuningVal * currentVal)"
                case .devide:
                    self.value = "\(ruuningVal % currentVal)"
                case .none:
                    break
                case .equal:
                    break
                }
            }
            if button != .equal {
                self.value = "0"
            }
        case .clear:
            self.value = "0"
            break
            
        case .decimal,.negtive,.precent:
            break
            
        default:
            let number = button.rawValue
            if self.value == "0"{
                value = number
            }else{
                self.value = "\(value)\(number)"
            }
            
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

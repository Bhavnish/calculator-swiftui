//
//  CalculatorApp.swift
//  Calculator
//
//  Created by cqlapple on 13/06/21.
//

import SwiftUI

@main
struct CalculatorApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
